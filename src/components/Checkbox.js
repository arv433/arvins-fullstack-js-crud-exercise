import React from 'react'

const Checkbox = props => (
    // Make this checkbox selectable based on whether if this row is being edited
    <div
        // Make the checkmark animate on hover if its row is editable
        className={`checkmarkContainer unselectable ${props.editable && "selectableCheckmark"}`}
        // If the checkmark is in an editable state, call the parent handler
        onClick={() => props.editable && props.changeCheckboxValue()}
    >
        <input
            type="checkbox"
            checked={props.checked}
            className="checkboxInput"
            // `readOnly` set to suppress checkmark warning - Does not affect custom behavior
            readOnly
        />
        <span className="checkmark"></span>
    </div>
)

export default Checkbox