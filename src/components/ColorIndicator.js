import React from 'react'

const ColorIndicator = props => (
    <div className="fullSizeCellWrapper">
        <button
            // Animate the Color indicator on hover if its row is editable
            className={`colorIndicatorContainer ${props.editable && "mutableColorIndicator"}`}
            // If the Color indicator is in an editable state, call the parent handler
            onClick={() => props.editable && props.beginEditingColor()}
        >
            {/* Set the color of the indicator based on the parent's state */}
            <div className="innerColor" style={{
                background: props.color
            }} />
        </button>
    </div>
)

export default ColorIndicator