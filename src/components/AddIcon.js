import React from 'react'

const AddIcon = props => (
        <svg width={21} height={21} {...props}>
            <path
                d="M19.884 9.885H11.45V1.451a.784.784 0 0 0-1.566 0v8.434H1.45a.784.784 0 0 0 0 1.566h8.434v8.434a.784.784 0 0 0 1.566 0V11.45h8.434a.784.784 0 0 0 0-1.565z"
                fill="#000"
                fillRule="evenodd"
            />
        </svg>
)

export default AddIcon
