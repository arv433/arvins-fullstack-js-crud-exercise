import React from 'react'

import { HuePicker } from 'react-color'
import posed from 'react-pose'

// Animation states
const ColorPickerState = posed.div({
    visible: {
        opacity: 1,
        y: 0,
        transition: {
            type: 'spring',
            stiffness: 200
        }
    },
    hidden: {
        opacity: 0,
        y: 15,
        transition: {
            ease: 'easeOut'
        }
    }
})

const ColorPicker = props => (
    // Handle the animated state based on whether if the Color Picker is visible depending on the parent state
    <ColorPickerState pose={props.isVisible ? 'visible' : 'hidden'}>
        {/* Set this components handlers and initial color from the data based from the parent's state */}
        <HuePicker
            className="colorSlider"
            color={props.initialColor}
            onChange={props.variableColor}
        />
    </ColorPickerState>
)

export default ColorPicker