import React from 'react'

// SVG image for the pen icon that appears when hovering over a row
const PenIcon = props => (
  <svg width={20} height={21} {...props}>
    <path
      d="M3.17.5h.625l2.41 2.41-3.794 3.795L0 4.295V3.67L3.17.5zm15.089 14.464L20 20.478l-5.513-1.741L3.996 8.246 7.79 4.45 18.26 14.964z"
      fill="#000"
      fillRule="nonzero"
    />
  </svg>
)

export default PenIcon
