import React from 'react'

import posed from 'react-pose'

/* Make the input field animated depending on whether it is invalidated
The invalid state is conditionally applied and removed during `saveButtonClicked()` in the parent*/
const AnimatedInputField = posed.input({
    invalid: {
        scale: 1.035,
        transition: {
            type: 'spring',
            stiffness: 200,
            damping: 0
        }
    },
    normal: {
        scale: 1
    }
})

// Render text cells' contents. If that cell's row is currently being edited, render an editable text field instead
// Gets called when rendering cells with text content other than `id`
const TextCell = props => {
    // Set the column key of the cell being rendered, as well as the details of the currently edited row
    const { row, isEditingId, editedRow, invalidateFields } = props, column = props.row.column.id, invalid = invalidateFields.includes(row.column.id)
    // If this is row is the one being edited
    if (isEditingId === row.row.id) {
        // Assign the text for this cell
        let textContent
        if (editedRow != null) {
            textContent = editedRow[column]
        }
        console.log(row)
        // Render the editable field
        return (<div className="fullSizeCellWrapper">
            <AnimatedInputField
                pose={invalid ? "invalid" : "normal"}
                type="text"
                className={`editingTextField ${invalid && "invalidTextField"}`}
                value={textContent}
                placeholder={row.column.Header}
                // Set the state of the edited row at the column each time the text is edited
                onChange={event => {
                    editedRow[column] = event.target.value
                    props.setTextFieldValue(editedRow)
                }}
            />
        </div>)
    } else {
        // Otherwise return a regular text cell
        return (<div className="fullSizeCellWrapper">
            <div className="unselectable">
                {row.row[column]}
            </div>
        </div>)
    }
}

export default TextCell