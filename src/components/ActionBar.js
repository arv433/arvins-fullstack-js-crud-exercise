import React from 'react'

import posed from 'react-pose'

// Animation states
const ActionBarState = posed.div({
    visible: {
        opacity: 1,
        y: 0,
        transition: {
            type: 'spring',
            stiffness: 200
        }
    },
    hidden: {
        opacity: 0,
        y: 15,
        transition: {
            ease: 'easeOut'
        }
    }
})

const ActionBar = props => (
    // Handle the animated state based on whether if the Action Bar is visible depending on the parent state
    <ActionBarState className="editingActionBar" pose={props.isVisible ? 'visible' : 'hidden'}>
        {/* The three buttons contained within the Action Bar behave the exact same, only their handlers from the parent state differ */}
        <button
            className={`editingAction ${props.isVisible && "clickableEditingAction"}`}
            // Disable the button from being accidently being clicked if it is invisible
            disabled={!props.isVisible}
            onClick={() => props.save()}
        >
            Save
        </button>
        <span className="actionBarSeperator">
            |
        </span>
        {/* Hide or unhide the 'Delete' button based on the parent state; when adding a new row */}
        {!props.isDeleteButtonHidden && <span>
            <button
                className={`editingAction ${props.isVisible && "clickableEditingAction"}`}
                disabled={!props.isVisible}
                onClick={() => props.deleteRow()}
            >
                Delete Row
            </button>
            <span className="actionBarSeperator">
                |
            </span>
        </span>}
        <button
            className={`editingAction ${props.isVisible && "clickableEditingAction"}`}
            disabled={!props.isVisible}
            onClick={() => props.cancelEditingOnRow()}
        >
            Cancel
        </button>
    </ActionBarState>
)

export default ActionBar