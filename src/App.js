import React from 'react'

// CSS
import './App.css'

// External Components
import ReactTable from 'react-table'
import 'react-table/react-table.css'

// Custom Components
import TextCell from './components/TextCell'
import Checkbox from './components/Checkbox'
import ColorIndicator from './components/ColorIndicator'
import ColorPicker from './components/ColorPicker'
import colorToHex from './utils/colorNameToHex.json'
import ActionBar from './components/ActionBar'
import PenIcon from './components/PenIcon'
import AddIcon from './components/AddIcon'

// Utility Modules
import API from './utils/serverAPI'

class App extends React.Component {
  state = {
    employees: [],
    isEditingId: 0,
    hoveredRow: 0,
    editedRow: null,
    loadingTable: false,
    isEditingColor: false,
    isAddingRow: false,
    deleteButtonHidden: false,
    invalidateFields: [],
  }

  componentWillMount = () => {
    this.fetchTableData()
  }

  // Retrieve data from the server at the given route
  fetchTableData = () => {
    const { loadingTable, editedRow } = this.state
    // -- Boilerplace code --
    fetch('http://localhost:8080/api/employees')
      .then(response => response.json())
      .then(employees => {
        this.setState({ employees })
        // ----
        // if the loading overlay is visible, disable it once data is retrieved
        loadingTable && this.setState({ loadingTable: false })
        // Remove `editedRow`
        editedRow != null && this.setState({ editedRow: null })
      })
  }

  // Simple String manipulation from a key to a column-header's title
  getColumnTitle = key => {
    switch (key) {
      // Unless that key is ID...
      case 'id':
        return 'ID'
      default:
        // Return that key's name with the first letter capitalized
        return key.charAt(0).toUpperCase() + key.slice(1)
    }
  }

  // Function to enter "editing mode"
  beginEditing = row => {
    console.log(row)
    // Filter out the row with actual information as `coreRow`
    const { _index, _nestingLevel, _original, _subRows, _viewIndex, ...coreRow } = row.row
    this.setState({
      isEditingId: coreRow.id,
      editedRow: coreRow
    })
  }

  // Function to enter "editing mode" when adding a new row
  beginAdding = () => {
    // Calculate a temporary `id` that increments from the `id` of the last row in `state.employees` so that the new row is sorted at the end
    const { employees } = this.state, newRowId = employees[employees.length - 1].id + 1, addingRow = {
      // In this stage, the table temporarily treats the placeholder row as a new row in the state with the following values
      id: newRowId,
      name: "",
      code: "",
      profession: "",
      color: "#ff0000",
      city: "",
      branch: "",
      assigned: false,
    }
    // Add the new row upon entering "Adding Mode"
    employees.push(addingRow)
    this.setState({
      // The `editedRow` will be temporarily set as the placeholder row
      editedRow: addingRow,
      // `isAddingRow` is set to true so that the "false" `id` does not show, hiding the 'Delete' button is triggered in the action bar, among other minor configurations
      isAddingRow: true,
      // The placeholder row is be set as the row being edited
      isEditingId: newRowId,
      // The placeholder row is set as the `hoveredRow` so that the Pen Icon immediately jumps to it and becomes bold, similar to when a row is being edited
      hoveredRow: newRowId,
      /* The 'Delete' button hides immediately, but upon clicking 'Save' or 'Cancel' (hiding the 'Action Bar'), there is a minimal delay for when the deleteButton unhides itself
      This is because if this behavior is not implemented, the Delete button will reappear immediately when the 'Action Bar' begins to fade out */
      deleteButtonHidden: true,
    })
  }

  setTextFieldValue = editedRow => {
    this.setState({ editedRow })
  }

  // When any action bar button is pressed, modify the state to undo all states that cause the editing mode
  editingDidEnd = () => {
    const { isEditingColor, isAddingRow, employees } = this.state, newState = { isEditingId: 0 }
    if (isEditingColor) newState.isEditingColor = false
    // If the user is adding a row
    if (isAddingRow) {
      // Remove the last placeholder row
      employees.pop()
      // Falsify `state.isAddingRow`
      newState.isAddingRow = false
      // Delay the reappearance of the 'Delete' button so that it does not show while the Action Bar is fading out when saving or cancelling when adding a new row
      setTimeout(() => this.setState({ deleteButtonHidden: false }), 0.3 * 1000)
    }
    this.setState(newState)
  }

  // Assign the checkbox its opposite value
  changeCheckboxValue = () => {
    const { editedRow } = this.state
    editedRow.assigned = !editedRow.assigned
    this.setState({ editedRow: editedRow })
  }

  // Called when a color indicator is clicked on
  toggleEditingColor = () => {
    const { isEditingId, isEditingColor } = this.state
    // if editing is available, toggle the state of `isEditingColor`
    isEditingId > 0 && this.setState({ isEditingColor: !isEditingColor })
  }

  // Using the JSON comprised of hex-code equivalents to CSS color names, pass return a value to be processed by the color picker
  passColorValue = color => {
    // if it is already a hex code, return it
    if (/^#(?:[0-9a-f]{3}){1,2}$/i.test(color.toLowerCase())) {
      return color.toLowerCase()
    } else {
      // otherwise, find the hex equivalent and pass it
      return colorToHex[color.toLowerCase()].toLowerCase()
    }
  }

  // Passed to color picker component to modify the state's `editedRow.color` property
  handleColorChange = color => {
    const { editedRow } = this.state
    editedRow.color = color.hex
    this.setState({ editedRow })
  }

  // Called when the 'Save' button is clicked, the entry is valid, and a row is being added
  addRow = () => {
    this.setState({ loadingTable: true })
    API.addEmployee(this.state.editedRow).then(res => {
      // Reload the table and close editing controls
      this.fetchTableData()
      this.editingDidEnd()
      console.log(res.status)
    })
  }

  // Called when the 'Save' button is clicked, the entry is valid, and a row is being modified
  updateRow = () => {
    const { isEditingId, editedRow } = this.state
    this.setState({ loadingTable: true })
    // Make a PUT request to the API server passing the `id` of the row being edited, and the current value of `state.editedRow`
    API.updateEmployee(isEditingId, editedRow).then(res => {
      // Reload the table and close editing controls
      console.log(res.status)
      this.fetchTableData()
      this.editingDidEnd()
    }).catch(err => console.log(err))
  }

  // Called whenever the 'save' button is clicked
  saveButtonClicked = () => {
    const { editedRow, isAddingRow } = this.state, { id, color, assigned, ...textEntries } = editedRow
    const invalidateFields = Object.keys(textEntries).filter(textEntryKey => textEntries[textEntryKey].length === 0)
    if (invalidateFields.length > 0) {
      console.log(invalidateFields)
      this.setState({ invalidateFields })
      setTimeout(() => this.setState({ invalidateFields: [] }), 1.35 * 1000)
      // Add or Update row depending on `state.isAddingRow`
    } else isAddingRow ? this.addRow() : this.updateRow()
  }

  // Called when the 'Delete' button is clicked
  deleteRow = () => {
    this.editingDidEnd()
    this.setState({ loadingTable: true })
    // Make a DELETE request to the API server passing the `id` of the row to be removed
    API.deleteEmployee(this.state.isEditingId).then(res => {
      // Reload the table and close editing controls
      console.log(res.status)
      this.fetchTableData()
      this.editingDidEnd()
    })
  }

  // Called when the 'Seed' button is clicked, only when the table is empty
  seedTable = () => {
    // Make a GET request to the API server to populate the table from a server-side JSON model
    API.seed().then(res => {
      console.log(res.status)
      this.fetchTableData()
    })
  }

  render = () => {
    // Entirely deconstruct the state for easier usage
    const {
      employees,
      isEditingId,
      hoveredRow,
      editedRow,
      loadingTable,
      isEditingColor,
      isAddingRow,
      deleteButtonHidden,
    } = this.state

    return (
      <div className="App">
        {/* `AppHeader` section contains the title and editing controls when visible */}
        <div className="AppHeader">
          {/* Title */}
          <h1 className="unselectable">Plexxis Employees</h1>
          {/* Color Picker editing control; only visible when a row's color is selected to be edited.
          A color value is passed cverted to Hex, and a handler is passed to modify the App state */}
          <ColorPicker
            isVisible={isEditingColor > 0}
            initialColor={isEditingId > 0 && this.passColorValue(editedRow.color)}
            variableColor={isEditingId > 0 && this.handleColorChange.bind(this)}
          />
          {/* Action Bar editing control; only visible when a row is being edited.
          Handlers allow the user to save, cancel, or delete a row in editing */}
          <ActionBar
            isVisible={isEditingId > 0}
            save={this.saveButtonClicked.bind(this)}
            isAddingRow={isAddingRow}
            cancelEditingOnRow={this.editingDidEnd.bind(this)}
            deleteRow={this.deleteRow.bind(this)}
            isDeleteButtonHidden={deleteButtonHidden}
          />
        </div>

        <div className="tableWrapper">
          {/* Conditionally render the table if there are employees in the table list.
          Otherwise, render the 'Seed' button */}
          {employees.length !== 0 ? (<ReactTable
            // Show the ReactTable 'loading' overlay; occurs while data is being sent or requested between the API Server
            loading={loadingTable}
            data={employees}
            // Limit the page size to the amount of employees in the table
            pageSize={employees.length}
            // No need for paginations, resizeable, or sortable
            showPagination={false}
            resizable={false}
            sortable={false}
            onMouseLeave={() => {
              console.log("Mouse left")
              this.setState({ hoveredRow: 0 })
            }}
            // The Editing icon will appear over a table if that row is being hovered on, using the state property `hoveredRow`
            getTrProps={(state, rowInfo) => {
              return {
                onMouseEnter: () => {
                  if (isEditingId === 0) {
                    this.setState({ hoveredRow: rowInfo.index + 1 })
                  }
                }
              }
            }}
            // Conditionally render the cells:
            columns={Object.keys(employees[0]).map(key => {
              // Define the default configuration for every type of cell
              const cellObject = {
                Header: this.getColumnTitle(key),
                accessor: key,
              }
              // Customize the cell based on the column it is in
              switch (key) {
                // The `id` cell takes the following configurations...
                case "id":
                  cellObject.Cell = row => {
                    return (<div className="fullSizeCellWrapper idCellWrapper">
                      {/* Render the row number inside the cell */}
                      <p className="idNumber unselectable">
                        {(isAddingRow && isEditingId === row.value) ? <AddIcon className="inRowAddIcon" /> : row.value}
                      </p>
                      {/* Render an invisible div outside the cell to conditionally display the Editing icon */}
                      <div className="extendedHover">
                        <PenIcon
                          // Begin editing for the corresponding row when the icon is clicked on
                          onClick={() => this.beginEditing(row)}
                          // Conditionally render the icon black and unclickable when in editing mode
                          className={`penIcon ${(isEditingId === row.row.id && "penIconEditing") || (hoveredRow === row.row._index + 1 && "penIconHovered")}`}
                        />
                      </div>
                    </div>)
                  }
                  // Add specific styles for this cell
                  cellObject.style = {
                    overflow: "visible",
                    padding: 0,
                    display: "flex",
                    justifyContent: "right"
                  }
                  // Make this cell narrower than the others
                  cellObject.width = 50
                  break
                // For every cell that takes and displays text, other than the `id` column, render editable text cells
                case "name":
                case "code":
                case "profession":
                case "city":
                case "branch":
                  cellObject.Cell = row => {
                    const { isEditingId, editedRow, invalidateFields } = this.state
                    return <TextCell
                      row={row}
                      isEditingId={isEditingId}
                      editedRow={editedRow}
                      invalidateFields={invalidateFields}
                      setTextFieldValue={this.setTextFieldValue.bind(this)}
                    />
                  }
                  break
                // Render a button-like color cell indicating an employee's assigned color
                case "color":
                  cellObject.Cell = row => (
                    <ColorIndicator
                      // Only modifiable if its row is currently being edited
                      editable={isEditingId === row.row.id}
                      // If the cell's row is being edited, set the color to the edited color. Otherwise, set it to the stored value.
                      color={isEditingId === row.row.id ? editedRow.color : row.value}
                      // Handle the toggled color state
                      beginEditingColor={this.toggleEditingColor.bind(this)}
                    />)
                  break
                case "assigned":
                  cellObject.Cell = row => (<Checkbox
                    // Only modifiable if its row is currently being edited
                    editable={isEditingId === row.row.id}
                    // If the cell's row is being edited, set the checkmark to the edited value. Otherwise, set it to the stored value.
                    checked={isEditingId === row.row.id ? editedRow.assigned : row.value}
                    // Handle the toggled checked state
                    changeCheckboxValue={this.changeCheckboxValue.bind(this)}
                  />)
                  break
                default:
                  break
              }
              return cellObject
            })}
          // Render the button labelled 'Seed this Table!' if there is no Employee data
          />) : <button onClick={this.seedTable} className="seedButton">Seed this Table!</button>}
          <div className={`addButtonWrapper ${isAddingRow ? "hiddenAddButtonDuringAdding" : isEditingId > 0 && "hiddenAddButtonDuringEditing"}`}>
            <AddIcon
              className={`addButton ${isEditingId === 0 && "clickable"}`}
              onClick={isEditingId === 0 ? this.beginAdding : null}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default App