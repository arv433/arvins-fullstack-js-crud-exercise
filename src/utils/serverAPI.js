import axios from 'axios'

const BASEURL = "http://localhost:8080/api/"

export default {
    // GET request to the 'Seed' endpoint that populates data in the table
    seed: () => axios.get(`${BASEURL}seed`),
    // POST requrest to the 'Add' endpoint that adds a new Employee into the database
    addEmployee: (body) => axios.post(`${BASEURL}add`, body),
    // PUT request to the 'Update' endpoint that changes a row's value
    updateEmployee: (id, body) => axios.put(`${BASEURL}update/${id}`, body),
    // DELETE request to the 'Delete' endpoint that removes a row from the table
    deleteEmployee: id => axios.delete(`${BASEURL}delete/${id}`),
}