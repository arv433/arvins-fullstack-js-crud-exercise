# Plexxis Employees Table

_This README talks about __Design__, __Implementation__, and __Usage__._

The Plexxis Employees Table is an easy-to-use interface for Creating, Reading, Updating, and Deleting Employee information.

__Two things to note,__

1) This app was not mobile-optimized; it looks and responds best at a viewport width of at least 850px.

2) Since this app was created with Sequelize ORM, be sure to configure `/server/config/config.json` by changing the host, username, and password depending on your environment. Additionally, don't forget to run `schema.sql` to create the necessary database for Sequelize to use.

# Design

To create this application, I gave the most focus to enabling clean ease-of-use through a fluid, and familiar interface. One where a user could easily identify exactly what each of the controls would do so that understanding how to read and make adjustments to the table would feel like second nature.

To implement this design, I created familiar icons such as a pen for editing and a plus sign for adding rows. I placed controls very sparingly in order to avoid clutter/confusion as much as possible. For example, the color picker only appears, when the color indicator is clicked on during editing mode, or the 'Delete Row' button only appears next to 'Save' when a row is being modified. To draw a user's attention to a control, I used light UI animations, but only in instances where it was helpful to accessibility.

Other efforts when into making my work modular and comprehensive without using any extra state management dependencies. This is so that another developer could pick up where I left off more easily.

# Implementation

## Server

The server was implemented in a Node.js runtime using Express.js with CORS options enabled to send requests between ports 3000 and 8080. Data persistence is managed in MySQL with the use of the Sequelize ORM for the safe and easy handling of JavaScript objects converted to query-language statements whenever one of the Express-powered API routes are hit.

## Client

The client-side application is a Facebook/React app making use of a few different npm dependencies to emphasize the simplistic design and user-friendliness:

* When there is Employee data available, an attention-grabbing and easy readable table is rendered in the body of the page. The exact measures and customization is made possible with `react-table`.

* `react-color` provides a straight-forward color picker with a draggable knob to cycle through a color spectrum, suggesting that directly controls the currently-edited color.

* Light UI animations are implemented to graphically draw the user's attention to a section of the interface as a result of their previous interaction with the application. This is done with the framework `react-pose`. For example, the action bar fades in upwards (seemingly out of the table) when a user enters editing mode so that they become clearly aware of the new options available; 'Save', 'Delete' or 'Cancel' if they are modifying a row, or just 'Save' and 'Cancel' if they are adding a new row.

* `axios` is used to effectively communicate with server endpoints with various HTTP methods.

## File Tree
A file tree is shown below to illustrate the application's architecture:

![App File Tree](/images/tree_screenshot.png)

# Usage

Initially, as there are no Employees in the table, you will have the option to add an Employee or seed the table with seven predefined entries.

![Screenshot Main App](/images/screenshot.png)

The table can be changed by Creating, Updating, or Deleting a row.

* To Create, click the plus icon at the bottom of the table.

* To Update, hover over a row, click the 'Edit' icon, and click 'Save' after making adjustments.

* To Delete, hover over a row, click the 'Edit' icon, and click 'Delete Row'

Making adjustments when Creating and Updating goes as follows:
* Click any of text fields to write in text to be saved.

* Click the color indicator to reveal the color picker, the drag the knob across it to set a color for that row.

* Toggle the checkbox to set assignment to true or false.

![Screenshot Main App](/images/screenshot_adding.png)

_You can only make an adjustment when a row is being edited, signified by the boldened 'Edit' icon next to that row. If any text fields are left empty during ediitng, saving is disabled and clicking 'Save' will flash the empty text boxes._