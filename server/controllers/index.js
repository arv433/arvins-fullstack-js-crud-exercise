const cors = require('cors')

// Import database models
const db = require('../models')
const queryInterface = db.sequelize.getQueryInterface()

// Configure in-flight CORS options
const corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200
}

// Reduncy reduction; all five endpoints defined below use the same response pattern in this function:
const makeServerResponse = (dbAction, req, res, next) => {
    // If the Promise resolution was successful, respond positively with the action's result
    dbAction.then(dbResponse => {
        res.setHeader('Content-Type', 'application/json')
        res.status(200)
        res.json(dbResponse)
    // Handle errors if the Sequelize function fails by sending 500 status and logging the error to the console
    }).catch(err => {
        res.setHeader('Content-Type', 'application/json')
        res.status(500)
        if (err) console.log(err)
    })
}

// Assign routing functions
module.exports = app => {
    // Return All employees from API
    app.get('/api/employees', cors(corsOptions), (req, res) => {
        console.log('/api/employees')
        makeServerResponse(db.Employee.findAll(), req, res)
    })

    // Add Employees to the Table from prewritten JSON model
    app.get('/api/seed', cors(corsOptions), (req, res) => {
        console.log('/api/seed')
        // Get employees without the id key, Sequelize already automatically implements this
        const employees = require('../data/employees.json').map(dbRow => {
            const { id, ...employee } = dbRow
            return employee
        })
        // Insert the seven predefined records
        makeServerResponse(queryInterface.bulkInsert(
            "Employees",
            employees
        ), req, res)
    })

    // Add user-configured Employee row to the Table
    app.post('/api/add', cors(corsOptions), (req, res) => {
        console.log('/api/add')
        console.log(req.body)
        const { name, code, profession, color, city, branch, assigned } = req.body

        // Post each of the listed properties to the row
        makeServerResponse(db.Employee.create({
            name,
            code,
            profession,
            color,
            city,
            branch,
            city,
            assigned
        }), req, res)
    })

    // Update a row's details
    app.put('/api/update/:id', cors(corsOptions), (req, res) => {
        const updatingId = req.params.id
        console.log(`/api/update/${updatingId}`)
        console.log(req.body)
        makeServerResponse(db.Employee.update(req.body,
            {
                where: {
                    id: req.params.id
                }
            }), req, res)
    })

    // Remove a row
    app.delete('/api/delete/:id', cors(corsOptions), (req, res) => {
        const deletingId = req.params.id
        console.log(`/api/delete/${deletingId}`)
        makeServerResponse(db.Employee.destroy({
            where: {
                id: deletingId
            }
        }), req, res)
    })
}
