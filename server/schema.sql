-- Drops employees_db if it exists currently --
DROP DATABASE IF EXISTS employees_db;
-- Creates the employees_db database --
CREATE DATABASE employees_db;
