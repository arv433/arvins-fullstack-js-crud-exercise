const express = require('express')
const app = express()
const cors = require('cors')
const db = require('./models')

// install middleware
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// pre-flight CORS for POST, PUT, and DELETE requests
const corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}
app.options('/api/*', cors(corsOptions))

// Routing
require('./controllers')(app)

db.sequelize.sync().then(() => {
  // Listen on port 8080
  app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'))
})